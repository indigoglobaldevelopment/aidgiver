/**
 * Created by pajicv on 1/22/19.
 */
const axios = require('axios');
const fs = require('fs');

const User = require('../user/User');
const Application = require('../application/Application');

const getUUID = length => [...(new Array(length))].reduce((uuid, item) => uuid + "0123456789".charAt(Math.floor(Math.random() * 10)), '');

const capitalizeFirstLetter = string => string.charAt(0).toUpperCase() + string.toLowerCase().slice(1);

const removeTabs = string => {
    let result = string;
    while (result.indexOf('\t') > -1) {
        const end = result.indexOf('\t');
        result = string.substr(0, end);
    }
    return result;
};

const loadFirstNames = () => {

    return new Promise((resolve, reject) => {

        fs.readFile(__dirname + '/first-names.txt', 'utf-8', (error, data) => {
            if (error) {
                return reject(error);
            }

            const lines = data.split('\n');

            const firstNamesByFrequency = [];

            while (lines.length > 0) {
                const record = lines.splice(0, 4);
                for(let i = 0; i < record[1]; ++i) {
                    let firstName = removeTabs(record[3]);
                    firstName = capitalizeFirstLetter(firstName);
                    firstNamesByFrequency.push(firstName);
                }
            }

            resolve(firstNamesByFrequency);

        });

    });

};

const loadLastNames = () => {

    return new Promise((resolve, reject) => {

        fs.readFile(__dirname + '/last-names.txt', 'utf-8', (error, data) => {
            if (error) {
                return reject(error);
            }

            const lastNames = data.split(',').map(lastName => lastName.trim());

            resolve(lastNames);

        });

    });

};

const loadStateLocalGov = () => {

    return new Promise((resolve, reject) => {

        fs.readFile(__dirname + '/local_govs.txt', 'utf-8', (error, data) => {
            if (error) {
                return reject(error);
            }

            const rows = data.split('\n');

            const localGovs = rows.map(row => row.split(';')[0]);

            const states = rows.map(row => row.split(';')[1]);

            resolve({
                states,
                localGovs
            });

        });

    });

};


const getRandomName = names => names[ Math.floor(Math.random() * names.length) ];

const getUsername = (firstName, lastName) => firstName.toLowerCase() + lastName.toLowerCase();

const getRandomDate = (start, end) => {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
};

const password = '12345678';

const amounts = [];
for (let i = 1000; i < 10000; i += 500) {
    amounts.push(i * 100);
}

Promise.all([loadFirstNames(), loadLastNames(), loadStateLocalGov()])
    .then(([firstNames, lastNames, { states, localGovs }]) => {
        const firstName = getRandomName(firstNames);
        const lastName = getRandomName(lastNames);
        const username = getUsername(firstName, lastName);
        const uuid = getUUID(10);
        const stateId = getRandomName(states);
        const localGovId = getRandomName(localGovs);
        const amountAsked = getRandomName(amounts);
        const email = username + '@gmail.com';
        const dateOfSubmission = getRandomDate(new Date(2018, 0, 1), new Date());

        return User.create({
            firstName,
            lastName,
            username,
            password,
            uuid,
            stateId,
            localGovId,
            email
        })
            .then(userId => Application.create({ userId, amountAsked, dateOfSubmission}))
    })
.then(console.log)
.catch(console.log);
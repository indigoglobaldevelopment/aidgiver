--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.10
-- Dumped by pg_dump version 9.5.10

-- Started on 2019-01-17 11:53:44 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE gfpdb;
--
-- TOC entry 3216 (class 1262 OID 506408)
-- Name: gfpdb; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE gfpdb WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


\connect gfpdb

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 13318)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3219 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 198 (class 1259 OID 506617)
-- Name: application; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE application (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    date_of_submission timestamp with time zone NOT NULL,
    amount bigint NOT NULL,
    application_status_id bigint DEFAULT 3,
    CONSTRAINT application_amount_check CHECK ((amount > 0))
);


--
-- TOC entry 197 (class 1259 OID 506615)
-- Name: application_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3220 (class 0 OID 0)
-- Dependencies: 197
-- Name: application_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE application_id_seq OWNED BY application.id;


--
-- TOC entry 200 (class 1259 OID 506631)
-- Name: application_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE application_status (
    id integer NOT NULL,
    name character varying(20) NOT NULL
);


--
-- TOC entry 199 (class 1259 OID 506629)
-- Name: application_status_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE application_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3221 (class 0 OID 0)
-- Dependencies: 199
-- Name: application_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE application_status_id_seq OWNED BY application_status.id;


--
-- TOC entry 184 (class 1259 OID 506432)
-- Name: knex_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE knex_migrations (
    id integer NOT NULL,
    name character varying(255),
    batch integer,
    migration_time timestamp with time zone
);


--
-- TOC entry 183 (class 1259 OID 506430)
-- Name: knex_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE knex_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3223 (class 0 OID 0)
-- Dependencies: 183
-- Name: knex_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE knex_migrations_id_seq OWNED BY knex_migrations.id;


--
-- TOC entry 186 (class 1259 OID 506440)
-- Name: knex_migrations_lock; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE knex_migrations_lock (
    index integer NOT NULL,
    is_locked integer
);


--
-- TOC entry 185 (class 1259 OID 506438)
-- Name: knex_migrations_lock_index_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE knex_migrations_lock_index_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3225 (class 0 OID 0)
-- Dependencies: 185
-- Name: knex_migrations_lock_index_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE knex_migrations_lock_index_seq OWNED BY knex_migrations_lock.index;


--
-- TOC entry 182 (class 1259 OID 506420)
-- Name: local_gov; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE local_gov (
    id integer NOT NULL,
    name character varying(255),
    state_id integer
);


--
-- TOC entry 202 (class 1259 OID 506646)
-- Name: payment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment (
    id integer NOT NULL,
    lot character varying(255) NOT NULL,
    date_of_payment timestamp with time zone NOT NULL,
    total_amount bigint NOT NULL,
    comment character varying(1023)
);


--
-- TOC entry 201 (class 1259 OID 506644)
-- Name: payment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3227 (class 0 OID 0)
-- Dependencies: 201
-- Name: payment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_id_seq OWNED BY payment.id;


--
-- TOC entry 190 (class 1259 OID 506471)
-- Name: sale_point; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sale_point (
    id integer NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    state_id integer,
    local_gov_id integer,
    name character varying(255) NOT NULL,
    uuid character varying(255),
    address character varying(255),
    city character varying(255),
    country character varying(255),
    phone character varying(255),
    latitude real,
    longitude real,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now()
);


--
-- TOC entry 194 (class 1259 OID 506581)
-- Name: sale_point_wallet; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sale_point_wallet (
    id integer NOT NULL,
    sale_point_id bigint NOT NULL,
    balance numeric(15,0),
    currency character varying(255),
    decimals integer DEFAULT 2,
    CONSTRAINT balance_min_check_constraint CHECK ((balance >= (0)::numeric))
);


--
-- TOC entry 193 (class 1259 OID 506579)
-- Name: sale_point_wallet_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sale_point_wallet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3229 (class 0 OID 0)
-- Dependencies: 193
-- Name: sale_point_wallet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sale_point_wallet_id_seq OWNED BY sale_point_wallet.id;


--
-- TOC entry 189 (class 1259 OID 506469)
-- Name: sales_point_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sales_point_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3230 (class 0 OID 0)
-- Dependencies: 189
-- Name: sales_point_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sales_point_id_seq OWNED BY sale_point.id;


--
-- TOC entry 181 (class 1259 OID 506412)
-- Name: state; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE state (
    id integer NOT NULL,
    name character varying(255)
);


--
-- TOC entry 196 (class 1259 OID 506595)
-- Name: trade; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE trade (
    id integer NOT NULL,
    user_wallet_id bigint NOT NULL,
    sale_point_wallet_id bigint NOT NULL,
    amount numeric(15,0),
    created_at timestamp with time zone DEFAULT now(),
    CONSTRAINT amount_min_check_constraint CHECK ((amount >= (0)::numeric))
);


--
-- TOC entry 195 (class 1259 OID 506593)
-- Name: trade_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE trade_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3232 (class 0 OID 0)
-- Dependencies: 195
-- Name: trade_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE trade_id_seq OWNED BY trade.id;


--
-- TOC entry 188 (class 1259 OID 506448)
-- Name: user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "user" (
    id integer NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    state_id integer,
    local_gov_id integer,
    title character varying(255),
    first_name character varying(255),
    middle_name character varying(255),
    last_name character varying(255),
    uuid character varying(255),
    address character varying(255),
    city character varying(255),
    country character varying(255),
    phone character varying(255),
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now()
);


--
-- TOC entry 187 (class 1259 OID 506446)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3234 (class 0 OID 0)
-- Dependencies: 187
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- TOC entry 192 (class 1259 OID 506524)
-- Name: user_wallet; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_wallet (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    balance numeric(15,0),
    currency character varying(255),
    decimals integer DEFAULT 2,
    CONSTRAINT balance_min_check_constraint CHECK ((balance >= (0)::numeric))
);


--
-- TOC entry 191 (class 1259 OID 506522)
-- Name: user_wallet_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_wallet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3235 (class 0 OID 0)
-- Dependencies: 191
-- Name: user_wallet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_wallet_id_seq OWNED BY user_wallet.id;


--
-- TOC entry 3024 (class 2604 OID 506620)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY application ALTER COLUMN id SET DEFAULT nextval('application_id_seq'::regclass);


--
-- TOC entry 3027 (class 2604 OID 506634)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY application_status ALTER COLUMN id SET DEFAULT nextval('application_status_id_seq'::regclass);


--
-- TOC entry 3007 (class 2604 OID 506435)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY knex_migrations ALTER COLUMN id SET DEFAULT nextval('knex_migrations_id_seq'::regclass);


--
-- TOC entry 3008 (class 2604 OID 506443)
-- Name: index; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY knex_migrations_lock ALTER COLUMN index SET DEFAULT nextval('knex_migrations_lock_index_seq'::regclass);


--
-- TOC entry 3028 (class 2604 OID 506649)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment ALTER COLUMN id SET DEFAULT nextval('payment_id_seq'::regclass);


--
-- TOC entry 3012 (class 2604 OID 506474)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sale_point ALTER COLUMN id SET DEFAULT nextval('sales_point_id_seq'::regclass);


--
-- TOC entry 3018 (class 2604 OID 506584)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sale_point_wallet ALTER COLUMN id SET DEFAULT nextval('sale_point_wallet_id_seq'::regclass);


--
-- TOC entry 3021 (class 2604 OID 506598)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY trade ALTER COLUMN id SET DEFAULT nextval('trade_id_seq'::regclass);


--
-- TOC entry 3009 (class 2604 OID 506451)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- TOC entry 3015 (class 2604 OID 506527)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_wallet ALTER COLUMN id SET DEFAULT nextval('user_wallet_id_seq'::regclass);


--
-- TOC entry 3207 (class 0 OID 506617)
-- Dependencies: 198
-- Data for Name: application; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO application (id, user_id, date_of_submission, amount, application_status_id) VALUES (7, 13, '2019-01-15 17:13:46.351409+01', 225000, 3);
INSERT INTO application (id, user_id, date_of_submission, amount, application_status_id) VALUES (10, 11, '2018-10-11 00:00:00+02', 120000, 3);
INSERT INTO application (id, user_id, date_of_submission, amount, application_status_id) VALUES (4, 11, '2019-01-15 12:14:17.846058+01', 100000, 1);
INSERT INTO application (id, user_id, date_of_submission, amount, application_status_id) VALUES (6, 12, '2019-01-15 17:13:18.725525+01', 350000, 1);


--
-- TOC entry 3236 (class 0 OID 0)
-- Dependencies: 197
-- Name: application_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('application_id_seq', 10, true);


--
-- TOC entry 3209 (class 0 OID 506631)
-- Dependencies: 200
-- Data for Name: application_status; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO application_status (id, name) VALUES (1, 'accepted');
INSERT INTO application_status (id, name) VALUES (2, 'rejected');
INSERT INTO application_status (id, name) VALUES (3, 'pending');


--
-- TOC entry 3237 (class 0 OID 0)
-- Dependencies: 199
-- Name: application_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('application_status_id_seq', 3, true);


--
-- TOC entry 3193 (class 0 OID 506432)
-- Dependencies: 184
-- Data for Name: knex_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO knex_migrations (id, name, batch, migration_time) VALUES (1, '20190103163414_create_users_table.js', 1, '2019-01-03 17:04:30.63+01');
INSERT INTO knex_migrations (id, name, batch, migration_time) VALUES (2, '20190103212511_create_sales_point_table.js', 2, '2019-01-03 21:34:05.116+01');
INSERT INTO knex_migrations (id, name, batch, migration_time) VALUES (3, '20190103215510_add_unique_constraints.js', 3, '2019-01-03 21:59:58.686+01');


--
-- TOC entry 3238 (class 0 OID 0)
-- Dependencies: 183
-- Name: knex_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('knex_migrations_id_seq', 3, true);


--
-- TOC entry 3195 (class 0 OID 506440)
-- Dependencies: 186
-- Data for Name: knex_migrations_lock; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO knex_migrations_lock (index, is_locked) VALUES (1, 0);


--
-- TOC entry 3239 (class 0 OID 0)
-- Dependencies: 185
-- Name: knex_migrations_lock_index_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('knex_migrations_lock_index_seq', 1, true);


--
-- TOC entry 3191 (class 0 OID 506420)
-- Dependencies: 182
-- Data for Name: local_gov; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO local_gov (id, name, state_id) VALUES (5353, 'Aba North', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5354, 'Aba South', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5355, 'Arochukw', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5356, 'Bende', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5357, 'Ikwuano', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5358, 'Isiala Ngwa North', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5359, 'Isiala Ngwa South', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5360, 'Isuikwua', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5361, 'Oboma Ngwa', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5362, 'Ohafia Abia', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5363, 'Osisioma Ngwa', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5364, 'Ugwunagbo', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5365, 'Ukwa East', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5366, 'Ukwa West', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5367, 'Umu-Nneochi', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5368, 'Umuahia North', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5369, 'Umuahia South', 215);
INSERT INTO local_gov (id, name, state_id) VALUES (5370, 'Demsa', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5371, 'Fufore', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5372, 'Ganye', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5373, 'Girie', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5374, 'Gombi', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5375, 'Guyuk', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5376, 'Hong', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5377, 'Jada', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5378, 'Lamurde', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5379, 'Madagali', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5380, 'Maiha', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5381, 'Mayo-Bel', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5382, 'Michika', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5383, 'Mubi North', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5384, 'Mubi South', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5385, 'Numan', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5386, 'Shelleng', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5387, 'Song', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5388, 'Teungo', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5389, 'Yola North', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5390, 'Yola South', 216);
INSERT INTO local_gov (id, name, state_id) VALUES (5391, 'Abak', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5392, 'Eastern Obolo', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5393, 'Eket', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5394, 'Esit Eket', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5395, 'Essien-U', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5396, 'EtimEkpo', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5397, 'Etinan', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5398, 'Ibeno', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5399, 'Ibesikpo Asutan', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5400, 'Ibiono Ibom', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5401, 'Ika', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5402, 'Ikono', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5403, 'Ikot-Aba', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5404, 'Ikot-Ekp', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5405, 'Ini', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5406, 'Itu', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5407, 'Mbo', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5408, 'Mkpat Enin', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5409, 'Nsit Atai', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5410, 'Nsit Ibom', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5411, 'Nsit Ubium', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5412, 'Obot Akara', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5413, 'Okobo', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5414, 'Onna', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5415, 'Oron', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5416, 'Oruk-Ana', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5417, 'Udung Uko', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5418, 'Ukanafun', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5419, 'Uruan', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5420, 'UrueOffo', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5421, 'Uyo', 217);
INSERT INTO local_gov (id, name, state_id) VALUES (5422, 'Aguata', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5423, 'Anambra East', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5424, 'Anambra West', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5425, 'Anaocha', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5426, 'AwkaNort', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5427, 'AwkaSout', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5428, 'Ayamelum', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5429, 'Dunukofia', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5430, 'Ekwusigo', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5431, 'Idemili North', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5432, 'Idemili South', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5433, 'Ihiala', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5434, 'Njikoka', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5435, 'NnewiNort', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5436, 'NnewiSou', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5437, 'Ogbaru', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5438, 'Onitsha North', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5439, 'Onitsha South', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5440, 'OrumbaNo', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5441, 'OrumbaSo', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5442, 'Oyi', 218);
INSERT INTO local_gov (id, name, state_id) VALUES (5443, 'Alkaleri', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5444, 'Bauchi', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5445, 'Bogoro', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5446, 'Damban', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5447, 'Darazo', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5448, 'Dass', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5449, 'Gamawa', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5450, 'Gamjuwa', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5451, 'Giade', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5452, 'Itas/Gad', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5453, 'Jama''are', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5454, 'Katagum', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5455, 'Kirfi', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5456, 'Misau', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5457, 'Ningi', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5458, 'Shira', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5459, 'Tafawa-B', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5460, 'Toro', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5461, 'Warji', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5462, 'Zaki', 219);
INSERT INTO local_gov (id, name, state_id) VALUES (5463, 'Brass', 220);
INSERT INTO local_gov (id, name, state_id) VALUES (5464, 'Ekeremor', 220);
INSERT INTO local_gov (id, name, state_id) VALUES (5465, 'Kolokuma/Opokuma', 220);
INSERT INTO local_gov (id, name, state_id) VALUES (5466, 'Nembe', 220);
INSERT INTO local_gov (id, name, state_id) VALUES (5467, 'Ogbia', 220);
INSERT INTO local_gov (id, name, state_id) VALUES (5468, 'Sagbama', 220);
INSERT INTO local_gov (id, name, state_id) VALUES (5469, 'Southern Ijaw', 220);
INSERT INTO local_gov (id, name, state_id) VALUES (5470, 'Yenegoa', 220);
INSERT INTO local_gov (id, name, state_id) VALUES (5471, 'Ado', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5472, 'Agatu', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5473, 'Apa', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5474, 'Buruku', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5475, 'Gboko', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5476, 'Guma', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5477, 'Gwer East', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5478, 'GwerWest', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5479, 'Katsina (Benue)', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5480, 'Konshish', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5481, 'Kwande', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5482, 'Logo', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5483, 'Makurdi', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5484, 'Obi', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5485, 'Ogbadibo', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5486, 'Ohimini', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5487, 'Oju', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5488, 'Okpokwu', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5489, 'Oturkpo', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5490, 'Tarka', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5491, 'Ukum', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5492, 'Ushongo', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5493, 'Vandeiky', 221);
INSERT INTO local_gov (id, name, state_id) VALUES (5494, 'Abadam', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5495, 'Askira/U', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5496, 'Bama', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5497, 'Bayo', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5498, 'Biu', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5499, 'Chibok', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5500, 'Damboa', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5501, 'Dikwa', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5502, 'Gubio', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5503, 'Guzamala', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5504, 'Gwoza', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5505, 'Hawul', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5506, 'Jere', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5507, 'Kaga', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5508, 'Kala/Balge', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5509, 'Konduga', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5510, 'Kukawa', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5511, 'Kwaya Kusar', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5512, 'Lake Chad', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5513, 'Mafa', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5514, 'Magumeri', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5515, 'Maidugur', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5516, 'Marte', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5517, 'Mobbar', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5518, 'Monguno', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5519, 'Ngala', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5520, 'Nganzai', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5521, 'Shani', 222);
INSERT INTO local_gov (id, name, state_id) VALUES (5522, 'Abi', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5523, 'Akamkpa', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5524, 'Akpabuyo', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5525, 'Bakassi', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5526, 'Bekwarra', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5527, 'Biase', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5528, 'Boki', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5529, 'Calabar South', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5530, 'Calabar', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5531, 'Etung', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5532, 'Ikom', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5533, 'Obanliku', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5534, 'Obubra', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5535, 'Obudu', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5536, 'Odukpani', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5537, 'Ogoja', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5538, 'Yakurr', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5539, 'Yala Cross', 223);
INSERT INTO local_gov (id, name, state_id) VALUES (5540, 'AniochaN', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5541, 'AniochaS', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5542, 'Bomadi', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5543, 'Burutu', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5544, 'Ethiope West', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5545, 'EthiopeE', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5546, 'IkaNorth', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5547, 'IkaSouth', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5548, 'IsokoNor', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5549, 'IsokoSou', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5550, 'Ndokwa East', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5551, 'Ndokwa West', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5552, 'Okpe', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5553, 'Oshimili North', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5554, 'Oshimili South', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5555, 'Patani', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5556, 'Sapele', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5557, 'Udu', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5558, 'Ughelli North', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5559, 'Ughelli South', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5560, 'Ukwuani', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5561, 'Uvwie', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5562, 'Warri North', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5563, 'Warri South-West', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5564, 'Warri South', 224);
INSERT INTO local_gov (id, name, state_id) VALUES (5565, 'Abakalik', 225);
INSERT INTO local_gov (id, name, state_id) VALUES (5566, 'Afikpo', 225);
INSERT INTO local_gov (id, name, state_id) VALUES (5567, 'AfikpoSo', 225);
INSERT INTO local_gov (id, name, state_id) VALUES (5568, 'Ebonyi', 225);
INSERT INTO local_gov (id, name, state_id) VALUES (5569, 'Ezza North', 225);
INSERT INTO local_gov (id, name, state_id) VALUES (5570, 'Ezza South', 225);
INSERT INTO local_gov (id, name, state_id) VALUES (5571, 'Ikwo', 225);
INSERT INTO local_gov (id, name, state_id) VALUES (5572, 'Ishielu', 225);
INSERT INTO local_gov (id, name, state_id) VALUES (5573, 'Ivo', 225);
INSERT INTO local_gov (id, name, state_id) VALUES (5574, 'Izzi', 225);
INSERT INTO local_gov (id, name, state_id) VALUES (5575, 'Ohaozara', 225);
INSERT INTO local_gov (id, name, state_id) VALUES (5576, 'Ohaukwu', 225);
INSERT INTO local_gov (id, name, state_id) VALUES (5577, 'Onicha', 225);
INSERT INTO local_gov (id, name, state_id) VALUES (5578, 'Akoko-Ed', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5579, 'Egor', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5580, 'EsanCent', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5581, 'EsanNort', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5582, 'EsanSout', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5583, 'EsanWest', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5584, 'Etsako Central', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5585, 'EtsakoEa', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5586, 'EtsakoWe', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5587, 'Igueben', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5588, 'Ikpoba-Okha', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5589, 'Oredo Edo', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5590, 'Orhionmw', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5591, 'OviaNort', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5592, 'OviaSouth-West', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5593, 'Owan East', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5594, 'OwanWest', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5595, 'Uhunmwonde', 226);
INSERT INTO local_gov (id, name, state_id) VALUES (5596, 'Ado-Ekiti', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5597, 'Efon', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5598, 'EkitiEas', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5599, 'EkitiSouth-West', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5600, 'EkitiWest', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5601, 'Emure/Ise/Orun', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5602, 'Gboyin', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5603, 'Ido/Osi', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5604, 'Ijero', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5605, 'Ikere', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5606, 'Ikole', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5607, 'Ilejemeje', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5608, 'Irepodun/Ifelodun', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5609, 'Ise/Orun', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5610, 'Moba', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5611, 'Oye', 227);
INSERT INTO local_gov (id, name, state_id) VALUES (5612, 'Aninri', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5613, 'Awgu', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5614, 'Enugu East', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5615, 'Enugu North', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5616, 'EnuguSou', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5617, 'Ezeagu', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5618, 'Igbo-Eti', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5619, 'Igbo-eze North', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5620, 'Igbo-eze South', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5621, 'Isi-Uzo', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5622, 'Nkanu East', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5623, 'Nkanu West', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5624, 'Nsukka', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5625, 'Oji-River', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5626, 'Udenu', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5627, 'Udi', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5628, 'Uzo-Uwani', 228);
INSERT INTO local_gov (id, name, state_id) VALUES (5629, 'Abaji', 229);
INSERT INTO local_gov (id, name, state_id) VALUES (5630, 'AbujaMun', 229);
INSERT INTO local_gov (id, name, state_id) VALUES (5631, 'Bwari', 229);
INSERT INTO local_gov (id, name, state_id) VALUES (5632, 'Gwagwala', 229);
INSERT INTO local_gov (id, name, state_id) VALUES (5633, 'Kuje', 229);
INSERT INTO local_gov (id, name, state_id) VALUES (5634, 'Kwali', 229);
INSERT INTO local_gov (id, name, state_id) VALUES (5635, 'Akko', 230);
INSERT INTO local_gov (id, name, state_id) VALUES (5636, 'Balanga', 230);
INSERT INTO local_gov (id, name, state_id) VALUES (5637, 'Billiri', 230);
INSERT INTO local_gov (id, name, state_id) VALUES (5638, 'Dukku', 230);
INSERT INTO local_gov (id, name, state_id) VALUES (5639, 'Funakaye', 230);
INSERT INTO local_gov (id, name, state_id) VALUES (5640, 'Gombe', 230);
INSERT INTO local_gov (id, name, state_id) VALUES (5641, 'Kaltungo', 230);
INSERT INTO local_gov (id, name, state_id) VALUES (5642, 'Kwami', 230);
INSERT INTO local_gov (id, name, state_id) VALUES (5643, 'Nafada', 230);
INSERT INTO local_gov (id, name, state_id) VALUES (5644, 'Shomgom', 230);
INSERT INTO local_gov (id, name, state_id) VALUES (5645, 'Yamaltu', 230);
INSERT INTO local_gov (id, name, state_id) VALUES (5646, 'Aboh-Mba', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5647, 'Ahizu-Mb', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5648, 'Ehime-Mb', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5649, 'Ezinihit', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5650, 'Ideato South', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5651, 'IdeatoNo', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5652, 'Ihitte/U', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5653, 'Ikeduru', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5654, 'IsialaMb', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5655, 'Isu', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5656, 'Mbaitoli', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5657, 'Ngor-Okp', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5658, 'Njaba', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5659, 'Nkwerre', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5660, 'Nwangele', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5661, 'Obowo', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5662, 'Oguta', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5663, 'Ohaji/Eg', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5664, 'Okigwe', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5665, 'Orlu', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5666, 'Orsu', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5667, 'Oru East', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5668, 'Oru West', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5669, 'Owerri Municipal', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5670, 'Owerri North', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5671, 'Owerri West', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5672, 'Unuimo', 231);
INSERT INTO local_gov (id, name, state_id) VALUES (5673, 'Auyo', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5674, 'Babura', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5675, 'Biriniwa', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5676, 'BirninKu', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5677, 'Buji', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5678, 'Dutse', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5679, 'Gagarawa', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5680, 'Garki', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5681, 'Gumel', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5682, 'Guri', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5683, 'Gwaram', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5684, 'Gwiwa', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5685, 'Hadejia', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5686, 'Jahun', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5687, 'KafinHau', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5688, 'Kaugama', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5689, 'Kazaure', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5690, 'KiriKasa', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5691, 'Kiyawa', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5692, 'Maigatari', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5693, 'MalamMad', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5694, 'Miga', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5695, 'Ringim', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5696, 'Roni', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5697, 'Sule-Tan', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5698, 'Taura', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5699, 'Yankwashi', 232);
INSERT INTO local_gov (id, name, state_id) VALUES (5700, 'Birnin-G', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5701, 'Chikun', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5702, 'Giwa', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5703, 'Igabi', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5704, 'Ikara', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5705, 'Jaba', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5706, 'Jema''a', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5707, 'Kachia', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5708, 'Kaduna North', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5709, 'Kaduna South', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5710, 'Kagarko', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5711, 'Kajuru', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5712, 'Kaura', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5713, 'Kauru', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5714, 'Kubau', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5715, 'Kudan', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5716, 'Lere', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5717, 'Makarfi', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5718, 'Sabon-Ga', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5719, 'Sanga', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5720, 'Soba', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5721, 'ZangonKa', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5722, 'Zaria', 233);
INSERT INTO local_gov (id, name, state_id) VALUES (5723, 'Ajingi', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5724, 'Albasu', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5725, 'Bagwai', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5726, 'Bebeji', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5727, 'Bichi', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5728, 'Bunkure', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5729, 'Dala', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5730, 'Dambatta', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5731, 'DawakinK', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5732, 'DawakinT', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5733, 'Doguwa', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5734, 'Fagge', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5735, 'Gabasawa', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5736, 'Garko', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5737, 'Garum Mallam', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5738, 'Gaya', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5739, 'Gezawa', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5740, 'Gwale', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5741, 'Gwarzo', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5742, 'Kabo', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5743, 'Kano', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5744, 'Karaye', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5745, 'Kibiya', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5746, 'Kiru', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5747, 'Kumbotso', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5748, 'Kunchi', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5749, 'Kura', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5750, 'Madobi', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5751, 'Makoda', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5752, 'Minjibir', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5753, 'Nassaraw', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5754, 'Rano', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5755, 'RiminGad', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5756, 'Rogo', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5757, 'Shanono', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5758, 'Sumaila', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5759, 'Takai', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5760, 'Tarauni', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5761, 'Tofa', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5762, 'Tsanyawa', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5763, 'Tundun Wada', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5764, 'Ungogo', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5765, 'Warawa', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5766, 'Wudil', 234);
INSERT INTO local_gov (id, name, state_id) VALUES (5767, 'Bakori', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5768, 'Batagarawa', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5769, 'Batsari', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5770, 'Baure', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5771, 'Bindawa', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5772, 'Charanchi', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5773, 'Dandume', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5774, 'Danja', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5775, 'Danmusa', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5776, 'Daura', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5777, 'Dutsi', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5778, 'Dutsin-M', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5779, 'Faskari', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5780, 'Funtua', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5781, 'Ingawa', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5782, 'Jibia', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5783, 'Kafur', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5784, 'Kaita', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5785, 'Kankara', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5786, 'Kankiya', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5787, 'Katsina (K)', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5788, 'Kurfi', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5789, 'Kusada', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5790, 'Mai''Adua', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5791, 'Malumfashi', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5792, 'Mani', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5793, 'Mashi', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5794, 'Matazu', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5795, 'Musawa', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5796, 'Rimi', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5797, 'Sabuwa', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5798, 'Safana', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5799, 'Sandamu', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5800, 'Zango', 235);
INSERT INTO local_gov (id, name, state_id) VALUES (5801, 'Aleiro', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5802, 'Arewa', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5803, 'Argungu', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5804, 'Augie', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5805, 'Bagudo', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5806, 'BirninKe', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5807, 'Bunza', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5808, 'Dandi', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5809, 'Danko Wasagu', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5810, 'Fakai', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5811, 'Gwandu', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5812, 'Jega', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5813, 'Kalgo', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5814, 'Koko/Bes', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5815, 'Maiyama', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5816, 'Ngaski', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5817, 'Sakaba', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5818, 'Shanga', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5819, 'Suru', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5820, 'Yauri', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5821, 'Zuru', 236);
INSERT INTO local_gov (id, name, state_id) VALUES (5822, 'Adavi', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5823, 'Ajaokuta', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5824, 'Ankpa', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5825, 'Bassa', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5826, 'Dekina', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5827, 'Ibaji', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5828, 'Idah', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5829, 'Igalamela-Odolu', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5830, 'Ijumu', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5831, 'Kabba/Bu', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5832, 'Kotonkar', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5833, 'Lokoja', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5834, 'Mopa-Muro', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5835, 'Ofu', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5836, 'Ogori/Magongo', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5837, 'Okehi', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5838, 'Okene', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5839, 'Olamabor', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5840, 'Omala', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5841, 'Yagba East', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5842, 'Yagba West', 237);
INSERT INTO local_gov (id, name, state_id) VALUES (5843, 'Asa', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5844, 'Baruten', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5845, 'Edu', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5846, 'Ekiti', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5847, 'Ifelodun', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5848, 'Ilorin East', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5849, 'Ilorin South', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5850, 'IlorinWe', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5851, 'Irepodun', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5852, 'Isin', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5853, 'Kaiama', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5854, 'Moro', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5855, 'Offa', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5856, 'Oke-Ero', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5857, 'Oyun', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5858, 'Pategi', 238);
INSERT INTO local_gov (id, name, state_id) VALUES (5859, 'Agege', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5860, 'Ajeromi/Ifelodun', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5861, 'Alimosho', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5862, 'Amuwo Odofin', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5863, 'Apapa', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5864, 'Badagary', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5865, 'Epe', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5866, 'Eti-Osa', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5867, 'Ibeju/Lekki', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5868, 'Ifako/Ijaye', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5869, 'Ikeja', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5870, 'Ikorodu', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5871, 'Kosofe', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5872, 'LagosIsland', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5873, 'Mainland', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5874, 'Mushin', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5875, 'Ojo', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5876, 'Oshodi/Isolo', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5877, 'Shomolu', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5878, 'Surulere', 239);
INSERT INTO local_gov (id, name, state_id) VALUES (5879, 'Akwanga', 240);
INSERT INTO local_gov (id, name, state_id) VALUES (5880, 'Awe', 240);
INSERT INTO local_gov (id, name, state_id) VALUES (5881, 'Doma', 240);
INSERT INTO local_gov (id, name, state_id) VALUES (5882, 'Karu', 240);
INSERT INTO local_gov (id, name, state_id) VALUES (5883, 'Keana', 240);
INSERT INTO local_gov (id, name, state_id) VALUES (5884, 'Keffi', 240);
INSERT INTO local_gov (id, name, state_id) VALUES (5885, 'Kokona', 240);
INSERT INTO local_gov (id, name, state_id) VALUES (5886, 'Lafia', 240);
INSERT INTO local_gov (id, name, state_id) VALUES (5887, 'Nasarawa', 240);
INSERT INTO local_gov (id, name, state_id) VALUES (5888, 'Nassarawa Egon', 240);
INSERT INTO local_gov (id, name, state_id) VALUES (5889, 'Obi', 240);
INSERT INTO local_gov (id, name, state_id) VALUES (5890, 'Toto', 240);
INSERT INTO local_gov (id, name, state_id) VALUES (5891, 'Wamba', 240);
INSERT INTO local_gov (id, name, state_id) VALUES (5892, 'Agaie', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5893, 'Agwara', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5894, 'Bida', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5895, 'Borgu', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5896, 'Bosso', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5897, 'Chanchaga', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5898, 'Edati', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5899, 'Gbako', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5900, 'Gurara', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5901, 'Katcha', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5902, 'Kontogur', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5903, 'Lapai', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5904, 'Lavun', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5905, 'Magama', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5906, 'Mariga', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5907, 'Mashegu', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5908, 'Mokwa', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5909, 'Muya', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5910, 'Paikoro', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5911, 'Rafi', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5912, 'Rijau', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5913, 'Shiroro', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5914, 'Suleja', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5915, 'Tafa', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5916, 'Wushishi', 241);
INSERT INTO local_gov (id, name, state_id) VALUES (5917, 'Abeokuta South', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5918, 'AbeokutaNorth', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5919, 'AdoOdo/Ota', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5920, 'EgbadoNorth', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5921, 'EgbadoSouth', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5922, 'Ewekoro', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5923, 'Ifo', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5924, 'Ijebu North-East', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5925, 'IjebuEast', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5926, 'IjebuNorth', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5927, 'IjebuOde', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5928, 'Ikenne', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5929, 'Imeko-Afon', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5930, 'Ipokia', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5931, 'Obafemi-Owode', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5932, 'Odeda', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5933, 'Odogbolu', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5934, 'OgunWaterside', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5935, 'Remo-North', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5936, 'Shagamu', 242);
INSERT INTO local_gov (id, name, state_id) VALUES (5937, 'Akoko North-East', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5938, 'Akoko South-East', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5939, 'Akoko South-West', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5940, 'AkokoNorthWest', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5941, 'Akure North', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5942, 'Akure South', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5943, 'Ese-Odo', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5944, 'Idanre', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5945, 'Ifedore', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5946, 'IlajeEseodo', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5947, 'IleOluji/Okeigbo', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5948, 'Irele', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5949, 'Odigbo', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5950, 'Okitipupa', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5951, 'Ondo East', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5952, 'Ondo West', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5953, 'Ose', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5954, 'Owo', 243);
INSERT INTO local_gov (id, name, state_id) VALUES (5955, 'Atakumosa East', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5956, 'Atakumosa West', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5957, 'Ayedaade', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5958, 'Ayedire', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5959, 'Boluwaduro', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5960, 'Boripe', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5961, 'Ede North', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5962, 'Ede South', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5963, 'Egbedore', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5964, 'Ejigbo', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5965, 'Ife East', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5966, 'Ife North', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5967, 'Ife South', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5968, 'IfeCentral', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5969, 'Ifedayo', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5970, 'Ifelodun', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5971, 'Ila', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5972, 'Ilesha East', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5973, 'Ilesha West', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5974, 'Irepodun', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5975, 'Irewole', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5976, 'Isokan', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5977, 'Iwo', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5978, 'Obokun', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5979, 'Odo0tin', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5980, 'Ola-Oluwa', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5981, 'Olorunda', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5982, 'Oriade', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5983, 'Orolu', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5984, 'Osogbo', 244);
INSERT INTO local_gov (id, name, state_id) VALUES (5985, 'Afijio', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5986, 'Akinyele', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5987, 'Atiba', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5988, 'Atisbo', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5989, 'Egbeda', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5990, 'IbadanNorth-East', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5991, 'IbadanNorth-West', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5992, 'IbadanNorth', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5993, 'IbadanSouth-East', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5994, 'IbadanSouth-West', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5995, 'Ibarapa Central', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5996, 'Ibarapa East', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5997, 'Ibarapa North', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5998, 'Ido', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (5999, 'Irepo', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6000, 'Iseyin', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6001, 'Itesiwaju', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6002, 'Iwajowa', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6003, 'Kajola', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6004, 'Lagelu', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6005, 'Ogbomosho North', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6006, 'Ogbomosho South', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6007, 'Ogo-Oluw', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6008, 'Olorunsogo', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6009, 'Oluyole', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6010, 'Ona-Ara', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6011, 'Orelope', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6012, 'Ori-Ire', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6013, 'Oyo East', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6014, 'Oyo West', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6015, 'Saki East', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6016, 'Saki West', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6017, 'Surulere', 245);
INSERT INTO local_gov (id, name, state_id) VALUES (6018, 'Barkin Ladi', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6019, 'Bassa', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6020, 'Bokkos', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6021, 'Jos East', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6022, 'Jos North', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6023, 'Jos South', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6024, 'Kanam', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6025, 'Kanke', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6026, 'Langtang North', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6027, 'Langtang South', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6028, 'Mangu', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6029, 'Mikang', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6030, 'Pankshin', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6031, 'Qua''anpa', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6032, 'Riyom', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6033, 'Shendam', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6034, 'Wase', 246);
INSERT INTO local_gov (id, name, state_id) VALUES (6035, 'Abua/Odu', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6036, 'Ahoada East', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6037, 'Ahoada West', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6038, 'Akukutor', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6039, 'Andoni/O', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6040, 'Asari-To', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6041, 'Bonny', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6042, 'Degema', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6043, 'Eleme', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6044, 'Emuoha', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6045, 'Etche', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6046, 'Gokana', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6047, 'Ikwerre', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6048, 'Khana', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6049, 'Obio/Akp', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6050, 'Ogba/Egbe', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6051, 'Ogu/Bolo', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6052, 'Okrika', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6053, 'Omumma', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6054, 'Opobo/Nkoro', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6055, 'Oyigbo', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6056, 'Port Harcourt', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6057, 'Tai', 247);
INSERT INTO local_gov (id, name, state_id) VALUES (6058, 'Binji', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6059, 'Bodinga', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6060, 'Dange-Shuni', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6061, 'Gada', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6062, 'Goronyo', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6063, 'Gudu', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6064, 'Gwadabaw', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6065, 'Illela', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6066, 'Isa', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6067, 'Kebbe', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6068, 'Kware', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6069, 'Rabah', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6070, 'Sabon Birni', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6071, 'Shagari', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6072, 'Silame', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6073, 'Sokoto North', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6074, 'Sokoto South', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6075, 'Tambawal', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6076, 'Tangazar', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6077, 'Tureta', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6078, 'Wamakko', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6079, 'Wurno', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6080, 'Yabo', 248);
INSERT INTO local_gov (id, name, state_id) VALUES (6081, 'Ardo-Kola', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6082, 'Bali', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6083, 'Donga', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6084, 'Gashaka', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6085, 'Gassol', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6086, 'Ibi', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6087, 'Jalingo', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6088, 'Karim-La', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6089, 'Kurmi', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6090, 'Lau', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6091, 'Sardauna', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6092, 'Takum', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6093, 'Ussa', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6094, 'Wukari', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6095, 'Yorro', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6096, 'Zing', 249);
INSERT INTO local_gov (id, name, state_id) VALUES (6097, 'Bade', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6098, 'Borsari', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6099, 'Damaturu', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6100, 'Fika', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6101, 'Fune', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6102, 'Geidam', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6103, 'Gujba', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6104, 'Gulani', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6105, 'Jakusko', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6106, 'Karasuwa', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6107, 'Machina', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6108, 'Nangere', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6109, 'Nguru', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6110, 'Potiskum', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6111, 'Tarmuwa', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6112, 'Yunusari', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6113, 'Yusufari', 250);
INSERT INTO local_gov (id, name, state_id) VALUES (6114, 'Anka', 251);
INSERT INTO local_gov (id, name, state_id) VALUES (6115, 'Bakura', 251);
INSERT INTO local_gov (id, name, state_id) VALUES (6116, 'Birnin-Magaji/Kiyaw', 251);
INSERT INTO local_gov (id, name, state_id) VALUES (6117, 'Bukkuyum', 251);
INSERT INTO local_gov (id, name, state_id) VALUES (6118, 'Bungudu', 251);
INSERT INTO local_gov (id, name, state_id) VALUES (6119, 'Gummi', 251);
INSERT INTO local_gov (id, name, state_id) VALUES (6120, 'Gusau', 251);
INSERT INTO local_gov (id, name, state_id) VALUES (6121, 'Kaura-Na', 251);
INSERT INTO local_gov (id, name, state_id) VALUES (6122, 'Maradun', 251);
INSERT INTO local_gov (id, name, state_id) VALUES (6123, 'Maru', 251);
INSERT INTO local_gov (id, name, state_id) VALUES (6124, 'Shinkafi', 251);
INSERT INTO local_gov (id, name, state_id) VALUES (6125, 'Talata-Mafara', 251);
INSERT INTO local_gov (id, name, state_id) VALUES (6126, 'Tsafe', 251);
INSERT INTO local_gov (id, name, state_id) VALUES (6127, 'Zurmi', 251);


--
-- TOC entry 3211 (class 0 OID 506646)
-- Dependencies: 202
-- Data for Name: payment; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO payment (id, lot, date_of_payment, total_amount, comment) VALUES (2, 'africa', '2019-01-17 10:22:49.734+01', 575000, '');
INSERT INTO payment (id, lot, date_of_payment, total_amount, comment) VALUES (3, 'Lot 1', '2018-12-31 00:00:00+01', 450000, 'Issued payment of subsidies for corn producers');


--
-- TOC entry 3240 (class 0 OID 0)
-- Dependencies: 201
-- Name: payment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('payment_id_seq', 3, true);


--
-- TOC entry 3199 (class 0 OID 506471)
-- Dependencies: 190
-- Data for Name: sale_point; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO sale_point (id, email, password, username, state_id, local_gov_id, name, uuid, address, city, country, phone, latitude, longitude, created_at, updated_at) VALUES (1, 'buhariobi@gmail.com', '$2a$08$2Rts0UqSuaLAWJVg31uHJO5d3zKzK4mL4KKrZmpOOTqllhG0UV4ji', 'buhariobi', 215, 5353, 'Fertilizers Abuja', '2345678901', 'Shettima A Munguno Street', 'Abuja', 'Nigeria', '+234 700 000 111', 9.06101894, 7.44645023, '2019-01-04 09:07:39.095272+01', '2019-01-04 09:07:39.095272+01');
INSERT INTO sale_point (id, email, password, username, state_id, local_gov_id, name, uuid, address, city, country, phone, latitude, longitude, created_at, updated_at) VALUES (8, 'danenesi@gmail.com', '$2a$08$XV5QJ89pg7xeS335A1BzSuFQhVN8FQfJ4pVt9Vf11JdMvjwHCl/oO', 'danenesi', 215, 5353, 'AgriFood Ltd', '3456789012', NULL, NULL, NULL, NULL, 9.01616955, 7.49244785, '2019-01-04 21:42:09.539193+01', '2019-01-04 21:42:09.539193+01');
INSERT INTO sale_point (id, email, password, username, state_id, local_gov_id, name, uuid, address, city, country, phone, latitude, longitude, created_at, updated_at) VALUES (10, 'sp333@gmail.com', '$2a$08$FDrqXBD/3FBe5j.YUyo7EeOgxxjIaz.TMXCW2VPOJyHwyikJh.7bC', 'sp333', 215, 5353, 'Fertilizers Abuja', '4567890123', NULL, NULL, NULL, NULL, 9.06101894, 7.44645023, '2019-01-08 11:02:21.219224+01', '2019-01-08 11:02:21.219224+01');


--
-- TOC entry 3203 (class 0 OID 506581)
-- Dependencies: 194
-- Data for Name: sale_point_wallet; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO sale_point_wallet (id, sale_point_id, balance, currency, decimals) VALUES (3, 8, 0, 'USD', 2);
INSERT INTO sale_point_wallet (id, sale_point_id, balance, currency, decimals) VALUES (4, 10, 200, 'USD', 2);


--
-- TOC entry 3241 (class 0 OID 0)
-- Dependencies: 193
-- Name: sale_point_wallet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('sale_point_wallet_id_seq', 4, true);


--
-- TOC entry 3242 (class 0 OID 0)
-- Dependencies: 189
-- Name: sales_point_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('sales_point_id_seq', 10, true);


--
-- TOC entry 3190 (class 0 OID 506412)
-- Dependencies: 181
-- Data for Name: state; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO state (id, name) VALUES (215, 'Abia');
INSERT INTO state (id, name) VALUES (216, 'Adamawa');
INSERT INTO state (id, name) VALUES (217, 'Akwa Ibom');
INSERT INTO state (id, name) VALUES (218, 'Anambra');
INSERT INTO state (id, name) VALUES (219, 'Bauchi');
INSERT INTO state (id, name) VALUES (220, 'Bayelsa');
INSERT INTO state (id, name) VALUES (221, 'Benue');
INSERT INTO state (id, name) VALUES (222, 'Borno');
INSERT INTO state (id, name) VALUES (223, 'Cross River');
INSERT INTO state (id, name) VALUES (224, 'Delta');
INSERT INTO state (id, name) VALUES (225, 'Ebonyi');
INSERT INTO state (id, name) VALUES (226, 'Edo');
INSERT INTO state (id, name) VALUES (227, 'Ekiti');
INSERT INTO state (id, name) VALUES (228, 'Enugu');
INSERT INTO state (id, name) VALUES (229, 'Federal Capital Territory');
INSERT INTO state (id, name) VALUES (230, 'Gombe');
INSERT INTO state (id, name) VALUES (231, 'Imo');
INSERT INTO state (id, name) VALUES (232, 'Jigawa');
INSERT INTO state (id, name) VALUES (233, 'Kaduna');
INSERT INTO state (id, name) VALUES (234, 'Kano');
INSERT INTO state (id, name) VALUES (235, 'Katsina');
INSERT INTO state (id, name) VALUES (236, 'Kebbi');
INSERT INTO state (id, name) VALUES (237, 'Kogi');
INSERT INTO state (id, name) VALUES (238, 'Kwara');
INSERT INTO state (id, name) VALUES (239, 'Lagos');
INSERT INTO state (id, name) VALUES (240, 'Nassarawa');
INSERT INTO state (id, name) VALUES (241, 'Niger');
INSERT INTO state (id, name) VALUES (242, 'Ogun');
INSERT INTO state (id, name) VALUES (243, 'Ondo');
INSERT INTO state (id, name) VALUES (244, 'Osun');
INSERT INTO state (id, name) VALUES (245, 'Oyo');
INSERT INTO state (id, name) VALUES (246, 'Plateau');
INSERT INTO state (id, name) VALUES (247, 'Rivers');
INSERT INTO state (id, name) VALUES (248, 'Sokoto');
INSERT INTO state (id, name) VALUES (249, 'Taraba');
INSERT INTO state (id, name) VALUES (250, 'Yobe');
INSERT INTO state (id, name) VALUES (251, 'Zamfara');


--
-- TOC entry 3205 (class 0 OID 506595)
-- Dependencies: 196
-- Data for Name: trade; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO trade (id, user_wallet_id, sale_point_wallet_id, amount, created_at) VALUES (1, 1, 4, 100, '2019-01-08 12:17:44.156653+01');
INSERT INTO trade (id, user_wallet_id, sale_point_wallet_id, amount, created_at) VALUES (2, 1, 4, 100, '2019-01-08 12:47:19.395262+01');


--
-- TOC entry 3243 (class 0 OID 0)
-- Dependencies: 195
-- Name: trade_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('trade_id_seq', 2, true);


--
-- TOC entry 3197 (class 0 OID 506448)
-- Dependencies: 188
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO "user" (id, email, password, username, state_id, local_gov_id, title, first_name, middle_name, last_name, uuid, address, city, country, phone, created_at, updated_at) VALUES (1, 'pajicv@gmail.com', '$2a$08$ves.uHkB4mlMMCNF2TDzIu/Y1oL2HwHCKv7AdwN/z30/1EwSpj7lu', 'pajicv', NULL, NULL, NULL, NULL, NULL, NULL, '1234567890', NULL, NULL, NULL, NULL, '2019-01-03 17:30:25.959918+01', '2019-01-03 17:30:25.959918+01');
INSERT INTO "user" (id, email, password, username, state_id, local_gov_id, title, first_name, middle_name, last_name, uuid, address, city, country, phone, created_at, updated_at) VALUES (5, 'test111@gmail.com', '$2a$08$.14wb7AALGojs7Gq7kQ1uuUswKVlUtwUWVZfQEJO87dzDv9u8OgPK', 'test111', NULL, NULL, NULL, NULL, NULL, NULL, '0123456789', NULL, NULL, NULL, NULL, '2019-01-03 22:34:43.803302+01', '2019-01-03 22:34:43.803302+01');
INSERT INTO "user" (id, email, password, username, state_id, local_gov_id, title, first_name, middle_name, last_name, uuid, address, city, country, phone, created_at, updated_at) VALUES (6, 'test222@gmail.com', '$2a$08$wnNMRbx1QUN8DcYsFbUmmOuh.FxY4eJFfRpWktGWDFOo6PGC0zPna', 'test222', NULL, NULL, NULL, NULL, NULL, NULL, '2345678901', NULL, NULL, NULL, NULL, '2019-01-04 08:40:53.622502+01', '2019-01-04 08:40:53.622502+01');
INSERT INTO "user" (id, email, password, username, state_id, local_gov_id, title, first_name, middle_name, last_name, uuid, address, city, country, phone, created_at, updated_at) VALUES (7, 'user333@gmail.com', '$2a$08$m/WMCumIxrrq7EV4j86YOOz6lmeTtzZPYmWP6pnmNm6wy6LrJS2Sy', 'user333', 215, 5353, NULL, NULL, NULL, NULL, '3456789012', NULL, NULL, NULL, NULL, '2019-01-08 11:02:30.203043+01', '2019-01-08 11:02:30.203043+01');
INSERT INTO "user" (id, email, password, username, state_id, local_gov_id, title, first_name, middle_name, last_name, uuid, address, city, country, phone, created_at, updated_at) VALUES (9, 'user444@gmail.com', '$2a$08$Lz0MTt3Ld8M3EgkwsH.xpu0ZA.Z673bOfsPGxYcmgnUo0YHVIB1x.', 'user444', 215, 5353, NULL, NULL, NULL, NULL, '4567890123', NULL, NULL, NULL, NULL, '2019-01-08 11:04:28.371966+01', '2019-01-08 11:04:28.371966+01');
INSERT INTO "user" (id, email, password, username, state_id, local_gov_id, title, first_name, middle_name, last_name, uuid, address, city, country, phone, created_at, updated_at) VALUES (11, 'yusufkazaure@gmail.com', '$2a$08$/pAosNHhqUYyjFIW2MJX1.c9lKApCEHaUPNNADbTD7JoWg6jyGWSS', 'yusufkazaure', 215, 5353, NULL, 'Yusuf', NULL, 'Kazaure', '5678901234', NULL, NULL, NULL, NULL, '2019-01-15 12:12:53.364938+01', '2019-01-15 12:12:53.364938+01');
INSERT INTO "user" (id, email, password, username, state_id, local_gov_id, title, first_name, middle_name, last_name, uuid, address, city, country, phone, created_at, updated_at) VALUES (12, 'olagokesamsonagbakosi@gmail.com', '$2a$08$uA5OJLih6yoqG.zXRy7rxunA.rpq5NvIxcMI0DH1UvTLLyUun4ZSO', 'olagokesamsonagbakosi', 215, 5353, NULL, 'Olagoke', 'Samson', 'Agbakosi', '6789012345', NULL, NULL, NULL, NULL, '2019-01-15 17:10:32.881401+01', '2019-01-15 17:10:32.881401+01');
INSERT INTO "user" (id, email, password, username, state_id, local_gov_id, title, first_name, middle_name, last_name, uuid, address, city, country, phone, created_at, updated_at) VALUES (13, 'owoichoigoji@gmail.com', '$2a$08$kEC2aDYFw4rjl9AwRYC9lO8kabHtv2eE97dH9g7OMMnahU8hEyVn2', 'owoichoigoji', 215, 5353, NULL, 'Owoicho', NULL, 'Igoji', '7890123456', NULL, NULL, NULL, NULL, '2019-01-15 17:12:24.240416+01', '2019-01-15 17:12:24.240416+01');


--
-- TOC entry 3244 (class 0 OID 0)
-- Dependencies: 187
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('user_id_seq', 13, true);


--
-- TOC entry 3201 (class 0 OID 506524)
-- Dependencies: 192
-- Data for Name: user_wallet; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO user_wallet (id, user_id, balance, currency, decimals) VALUES (1, 9, 800, 'USD', 2);
INSERT INTO user_wallet (id, user_id, balance, currency, decimals) VALUES (4, 13, 450000, 'USD', 2);
INSERT INTO user_wallet (id, user_id, balance, currency, decimals) VALUES (2, 11, 100000, 'USD', 2);
INSERT INTO user_wallet (id, user_id, balance, currency, decimals) VALUES (3, 12, 1050000, 'USD', 2);


--
-- TOC entry 3245 (class 0 OID 0)
-- Dependencies: 191
-- Name: user_wallet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('user_wallet_id_seq', 4, true);


--
-- TOC entry 3060 (class 2606 OID 506623)
-- Name: application_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY application
    ADD CONSTRAINT application_pkey PRIMARY KEY (id);


--
-- TOC entry 3062 (class 2606 OID 506636)
-- Name: application_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY application_status
    ADD CONSTRAINT application_status_pkey PRIMARY KEY (id);


--
-- TOC entry 3036 (class 2606 OID 506445)
-- Name: knex_migrations_lock_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY knex_migrations_lock
    ADD CONSTRAINT knex_migrations_lock_pkey PRIMARY KEY (index);


--
-- TOC entry 3034 (class 2606 OID 506437)
-- Name: knex_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY knex_migrations
    ADD CONSTRAINT knex_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 3032 (class 2606 OID 506429)
-- Name: local_gov_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY local_gov
    ADD CONSTRAINT local_gov_pkey PRIMARY KEY (id);


--
-- TOC entry 3064 (class 2606 OID 506654)
-- Name: payment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (id);


--
-- TOC entry 3056 (class 2606 OID 506587)
-- Name: sale_point_wallet_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sale_point_wallet
    ADD CONSTRAINT sale_point_wallet_pkey PRIMARY KEY (id);


--
-- TOC entry 3046 (class 2606 OID 506493)
-- Name: sales_point_email_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sale_point
    ADD CONSTRAINT sales_point_email_unique UNIQUE (email);


--
-- TOC entry 3048 (class 2606 OID 506481)
-- Name: sales_point_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sale_point
    ADD CONSTRAINT sales_point_pkey PRIMARY KEY (id);


--
-- TOC entry 3050 (class 2606 OID 506497)
-- Name: sales_point_username_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sale_point
    ADD CONSTRAINT sales_point_username_unique UNIQUE (username);


--
-- TOC entry 3052 (class 2606 OID 506501)
-- Name: sales_point_uuid_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sale_point
    ADD CONSTRAINT sales_point_uuid_unique UNIQUE (uuid);


--
-- TOC entry 3030 (class 2606 OID 506419)
-- Name: state_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY state
    ADD CONSTRAINT state_pkey PRIMARY KEY (id);


--
-- TOC entry 3058 (class 2606 OID 506601)
-- Name: trade_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trade
    ADD CONSTRAINT trade_pkey PRIMARY KEY (id);


--
-- TOC entry 3038 (class 2606 OID 506495)
-- Name: user_email_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_email_unique UNIQUE (email);


--
-- TOC entry 3040 (class 2606 OID 506458)
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 3042 (class 2606 OID 506499)
-- Name: user_username_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_username_unique UNIQUE (username);


--
-- TOC entry 3044 (class 2606 OID 506503)
-- Name: user_uuid_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_uuid_unique UNIQUE (uuid);


--
-- TOC entry 3054 (class 2606 OID 506530)
-- Name: user_wallet_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_wallet
    ADD CONSTRAINT user_wallet_pkey PRIMARY KEY (id);


--
-- TOC entry 3075 (class 2606 OID 506637)
-- Name: application_application_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY application
    ADD CONSTRAINT application_application_status_id_fkey FOREIGN KEY (application_status_id) REFERENCES application_status(id);


--
-- TOC entry 3074 (class 2606 OID 506624)
-- Name: application_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY application
    ADD CONSTRAINT application_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- TOC entry 3065 (class 2606 OID 506423)
-- Name: local_gov_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY local_gov
    ADD CONSTRAINT local_gov_state_id_fkey FOREIGN KEY (state_id) REFERENCES state(id);


--
-- TOC entry 3071 (class 2606 OID 506588)
-- Name: sale_point_wallet_sale_point_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sale_point_wallet
    ADD CONSTRAINT sale_point_wallet_sale_point_id_fkey FOREIGN KEY (sale_point_id) REFERENCES sale_point(id);


--
-- TOC entry 3069 (class 2606 OID 506487)
-- Name: sales_point_local_gov_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sale_point
    ADD CONSTRAINT sales_point_local_gov_id_foreign FOREIGN KEY (local_gov_id) REFERENCES local_gov(id);


--
-- TOC entry 3068 (class 2606 OID 506482)
-- Name: sales_point_state_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sale_point
    ADD CONSTRAINT sales_point_state_id_foreign FOREIGN KEY (state_id) REFERENCES state(id);


--
-- TOC entry 3073 (class 2606 OID 506607)
-- Name: trade_sale_point_wallet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trade
    ADD CONSTRAINT trade_sale_point_wallet_id_fkey FOREIGN KEY (sale_point_wallet_id) REFERENCES sale_point_wallet(id);


--
-- TOC entry 3072 (class 2606 OID 506602)
-- Name: trade_user_wallet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trade
    ADD CONSTRAINT trade_user_wallet_id_fkey FOREIGN KEY (user_wallet_id) REFERENCES user_wallet(id);


--
-- TOC entry 3067 (class 2606 OID 506464)
-- Name: user_local_gov_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_local_gov_id_foreign FOREIGN KEY (local_gov_id) REFERENCES local_gov(id);


--
-- TOC entry 3066 (class 2606 OID 506459)
-- Name: user_state_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_state_id_foreign FOREIGN KEY (state_id) REFERENCES state(id);


--
-- TOC entry 3070 (class 2606 OID 506531)
-- Name: user_wallet_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_wallet
    ADD CONSTRAINT user_wallet_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- TOC entry 3218 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 3222 (class 0 OID 0)
-- Dependencies: 184
-- Name: knex_migrations; Type: ACL; Schema: public; Owner: -
--

REVOKE ALL ON TABLE knex_migrations FROM PUBLIC;
REVOKE ALL ON TABLE knex_migrations FROM pajicv;
GRANT ALL ON TABLE knex_migrations TO pajicv;


--
-- TOC entry 3224 (class 0 OID 0)
-- Dependencies: 186
-- Name: knex_migrations_lock; Type: ACL; Schema: public; Owner: -
--

REVOKE ALL ON TABLE knex_migrations_lock FROM PUBLIC;
REVOKE ALL ON TABLE knex_migrations_lock FROM pajicv;
GRANT ALL ON TABLE knex_migrations_lock TO pajicv;


--
-- TOC entry 3226 (class 0 OID 0)
-- Dependencies: 182
-- Name: local_gov; Type: ACL; Schema: public; Owner: -
--

REVOKE ALL ON TABLE local_gov FROM PUBLIC;
REVOKE ALL ON TABLE local_gov FROM pajicv;
GRANT ALL ON TABLE local_gov TO pajicv;


--
-- TOC entry 3228 (class 0 OID 0)
-- Dependencies: 190
-- Name: sale_point; Type: ACL; Schema: public; Owner: -
--

REVOKE ALL ON TABLE sale_point FROM PUBLIC;
REVOKE ALL ON TABLE sale_point FROM pajicv;
GRANT ALL ON TABLE sale_point TO pajicv;


--
-- TOC entry 3231 (class 0 OID 0)
-- Dependencies: 181
-- Name: state; Type: ACL; Schema: public; Owner: -
--

REVOKE ALL ON TABLE state FROM PUBLIC;
REVOKE ALL ON TABLE state FROM pajicv;
GRANT ALL ON TABLE state TO pajicv;


--
-- TOC entry 3233 (class 0 OID 0)
-- Dependencies: 188
-- Name: user; Type: ACL; Schema: public; Owner: -
--

REVOKE ALL ON TABLE "user" FROM PUBLIC;
REVOKE ALL ON TABLE "user" FROM pajicv;
GRANT ALL ON TABLE "user" TO pajicv;


-- Completed on 2019-01-17 11:53:44 CET

--
-- PostgreSQL database dump complete
--


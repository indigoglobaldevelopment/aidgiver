/**
 * Created by pajicv on 1/3/19.
 */
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

router.use(bodyParser.json());
const Admin = require(__root + 'admin/Admin');

 router.post('/login', function(req, res) {

     Admin.login(req.body.username)
         .then(users => {

             if(users.length === 0) {
                 return res.status(400).send('User not found.');
             }

             //there can be only one user with the specified email
             const user = users[0];

             // check if the password is valid
             const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
             if (!passwordIsValid) return res.status(401).json({ auth: false, token: null });

             // if user is found and password is valid
             // create a token
             const token = jwt.sign({ id: user.id }, process.env.SECRET_KEY, {
                 expiresIn: 86400 // expires in 24 hours
             });

             // return the information including token as JSON
             res.status(200).json({ auth: true, token: token });

         })
         .catch(error => {
             console.log(error);
             res.status(500).json({message: 'Error on the server.'});
         });
 });

module.exports = router;
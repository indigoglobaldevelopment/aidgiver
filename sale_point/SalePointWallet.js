/**
 * Created by pajicv on 1/4/19.
 */
const knex = require('../db');

const TABLE_NAME = 'sale_point_wallet';

const get = id => knex(TABLE_NAME).select().where('id', '=', id).then(wallets => wallets[0]);

const getBySalePointId = (salePointId, trx) => knex(TABLE_NAME).select().transacting(trx).where('sale_point_id', '=', salePointId).then(wallets => wallets[0]);

const updateBalance = (id, amount, trx) => knex(TABLE_NAME).where('id', '=', id).increment('balance', amount).transacting(trx);

const updateBalanceForSalePoint = (salePointId, amount, trx) => knex(TABLE_NAME).where('sale_point_id', '=', salePointId).increment('balance', amount).transacting(trx);

const create = (salePointId, trx) => knex(TABLE_NAME).returning('id').insert({ sale_point_id: salePointId, currency: 'USD', balance: 0, decimals: 2 }).transacting(trx);

module.exports = { get, getBySalePointId, updateBalance, create, updateBalanceForSalePoint };
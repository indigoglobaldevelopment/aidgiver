/**
 * Created by pajicv on 1/3/19.
 */

const bcrypt = require("bcryptjs");

const knex = require("../db");

const UserWallet = require("./UserWallet");

const getAll = () =>
    knex
    .select(
        "user.id",
        "user.username",
        "user.email",
        { firstName: "user.first_name" },
        "user.uuid",
        { middleName: "user.middle_name" },
        { lastName: "user.last_name" },
        "user.title",
        "user.address",
        "user.city",
        "user.country",
        "user.phone",
        { amountAsked: "application.amount" },
        { dateOfSubmission: "application.date_of_submission" },
        { applicationId: "application.id" },
        { state: "state.name" },
        { localGov: "local_gov.name" }
    )
    .from("user")
    .innerJoin("application", "user.id", "application.user_id")
        .innerJoin("state", "state.id", "user.state_id")
        .innerJoin("local_gov", "local_gov.id", "user.local_gov_id")
        .innerJoin("application_status", "application_status.id", "application.application_status_id")
        .where('application_status.name', '=', 'pending');

const login = username =>
  knex
    .select(
      "user.id",
      "user.username",
      "user.password",
      "user.email",
      { firstName: "user.first_name" },
      "user.uuid",
      { middleName: "user.middle_name" },
      { lastName: "user.last_name" },
      "user.title",
      "user.address",
      "user.city",
      "user.country",
      "user.phone",
      /*{ walletId: "user_wallet.id" },
      { walletBalance: "user_wallet.balance" },
      { walletCurrency: "user_wallet.currency" },
      { walletDecimals: "user_wallet.decimals" }*/
    )
    .from("user")
    //.innerJoin("user_wallet", "user.id", "user_wallet.user_id")
    .where("username", "=", username);

const create = ({
  username,
  password,
  stateId,
  localGovId,
  title,
  firstName,
  middleName,
  lastName,
  uuid,
  address,
  city,
  country,
  phone,
  email
}) => {
  const hashedPassword = bcrypt.hashSync(password, 8);

    let userId;

  return knex.transaction(trx => {
    knex("user")
      .returning("id")
      .insert({
        username,
        password: hashedPassword,
        state_id: stateId,
        local_gov_id: localGovId,
        title,
        first_name: firstName,
        middle_name: middleName,
        last_name: lastName,
        uuid,
        address,
        city,
        country,
        phone,
        email
      })
      .then(ids => {
        userId = ids[0];
        return UserWallet.create(userId, trx);
      })
        .then(() => userId)
      .then(trx.commit)
      .catch(trx.rollback);
  });
};

module.exports = { getAll, login, create };

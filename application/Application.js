/**
 * Created by pajicv on 1/17/19.
 */

const knex = require("../db");

const monthNames = {
    "1": "Jan",
    "2": "Feb",
    "3": "Mar",
    "4": "Apr",
    "5": "May",
    "6": "Jun",
    "7": "Jul",
    "8": "Aug",
    "9": "Sep",
    "10": "Oct",
    "11": "Nov",
    "12": "Dec",
};

const updateStatus = (applicationId, newStatus, trx) => {
  return knex("application")
    .where("id", "=", applicationId)
    .update("application_status_id", newStatus)
    .transacting(trx);
};

const create = ({ userId, amountAsked, dateOfSubmission }) =>
  knex("application").insert({
    user_id: userId,
    amount: amountAsked,
    date_of_submission: dateOfSubmission
  });

const getLast12MonthsByAppStatus = () =>
  knex
    .raw(
      `
        SELECT 
            count(*) as number_of_applications,
            application_status_id,
            year || '-' || month AS month
        FROM ( 
            SELECT 
                *,
                date_part('month', date_of_submission) AS month,
                date_part('year', date_of_submission) AS year
            FROM 
                application 
            WHERE 
                date_of_submission > now() - interval '12 months' 
            ) AS applications_by_months
        GROUP BY 
            application_status_id, 
            year, 
            month
        ORDER BY 
            year DESC, 
            month DESC
      `
    )
    .then(result => {
        const today = new Date();
        let month = today.getMonth();
        let year = today.getFullYear();
        const months = [year.toString() + '-' + (month + 1).toString()];
        for(let i = 0; i < 11; ++i) {
            month -= 1;
            if(month < 0) {
                month = 11;
                year -= 1;
            }
            months.push(year.toString() + '-' + (month + 1).toString())
        }
        const applicationsByMonths = months.reduce((acc, month) => Object.assign(acc, { [month]: { approved: 0, rejected: 0, pending: 0 } }), {});
        console.log(applicationsByMonths);
        console.log(result.rows);
        result.rows.forEach(row => {
          if (!applicationsByMonths[row.month]) {
            return;
          }
            if(row.application_status_id === '1') {
                applicationsByMonths[row.month].approved = parseInt(row.number_of_applications)
            } else if (row.application_status_id === '2') {
                applicationsByMonths[row.month].rejected = parseInt(row.number_of_applications)
            } else {
                applicationsByMonths[row.month].pending = parseInt(row.number_of_applications)
            }
        });

        const data = Object.keys(applicationsByMonths)
            .reverse()
            .map(name => ({
                ...applicationsByMonths[name],
                name: monthNames[name.split('-')[1]]
            }));

        return data;
    });

const getTotalByAppStatus = () =>
  knex
    .raw(
      `
        SELECT 
            count(*), 
            application_status_id
        FROM 
            application
        GROUP BY 
            application_status_id
        ORDER BY 
            application_status_id
      `
    )
    .then(result => {
      console.log(result.rows);
      return result.rows;
    });

const getLast30DaysByAppStatus = () =>
  knex
    .raw(
      `
        SELECT 
	        application_status_id, 
	        count(*)
        FROM 
            application
        WHERE 
            date_of_submission > now() - interval '30 days'
        GROUP BY 
        application_status_id;
      `
    )
    .then(result => {
      console.log(result.rows);
      return result.rows;
    });

const getStates = () => knex('state').select({value: 'id'}, {label: 'name'});

const getLocalGovs = stateId => knex('local_gov').select({value: 'id'}, {label: 'name'}).where('state_id', '=', stateId);

module.exports = {
  updateStatus,
  create,
  getLast12MonthsByAppStatus,
  getLast30DaysByAppStatus,
  getTotalByAppStatus,
    getStates,
    getLocalGovs
};

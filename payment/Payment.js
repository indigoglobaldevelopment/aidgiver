/**
 * Created by pajicv on 1/17/19.
 */
const knex = require('../db');

const UserWallet = require('../user/UserWallet');

const Application = require('../application/Application');

const accept = ({ totalAmount, dateOfPayment, lot, comment }, applications) => {

    return knex.transaction(trx => {
        Promise.all(applications.map(({ id, amountAsked }) => UserWallet.getByUserId(id, trx)
            .then(userWallet => UserWallet.updateBalance(userWallet.id, amountAsked, trx))
        ))
            .then(() => {
                return Promise.all( applications.map(({ applicationId }) => Application.updateStatus(applicationId, 1, trx)));
            })
            .then(() =>  {
                return knex('payment')
                    .returning('id')
                    .insert({
                        total_amount: totalAmount,
                        date_of_payment: dateOfPayment,
                        lot,
                        comment
                    })
                    .transacting(trx)
            })
            .then(trx.commit)
            .catch(trx.rollback);
    });


};

const reject = (applications) => knex.transaction(trx => {
        return Promise.all(applications.map(({applicationId}) => Application.updateStatus(applicationId, 2, trx)))
            .then(trx.commit)
            .catch(trx.rollback);
    });

const getAll = () =>
    knex('payment')
        .select(
            'id',
            'lot',
            { dateOfPayment: "date_of_payment" },
            { totalAmount: "total_amount" },
            'comment'
        );

module.exports = { accept, reject, getAll };
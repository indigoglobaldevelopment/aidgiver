/**
 * Created by pajicv on 1/8/19.
 */
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

router.use(bodyParser.json());

const Transaction = require('./Transaction');

router.post('/', function (req, res) {

    const userId = 9;
    const salePointId = req.body.salePointId;
    const amount = req.body.amount;

    Transaction.create(userId, salePointId, amount)
        .then(result => {
            console.log(result);
            res.status(200).json({message: 'Transaction successful'})
        })
        .catch(error => {
            console.log(error);
            res.status(400).json({message: error.message})
        });

});

router.get('/user/:userId', function (req, res) {

    const userId = req.params.userId;

    Transaction.getHistoryForUser(userId)
        .then(result => {
            console.log(result);
            res.status(200).json(result)
        })
        .catch(error => {
            console.log(error);
            res.status(400).json({message: error.message})
        });

});

router.get('/sale_point/:salePointId', function (req, res) {

    const salePointId = req.params.salePointId;

    Transaction.getHistoryForSalePoint(salePointId)
        .then(result => {
            console.log(result);
            res.status(200).json(result)
        })
        .catch(error => {
            console.log(error);
            res.status(400).json({message: error.message})
        });

});

module.exports = router;
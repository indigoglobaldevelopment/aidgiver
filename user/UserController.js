/**
 * Created by pajicv on 1/3/19.
 */
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

router.use(bodyParser.json());
const User = require('./User');
const UserWallet = require('./UserWallet');

// CREATES A NEW USER
router.post('/', function (req, res) {

    const { username, password, stateId, localGovId, title, firstName, middleName, lastName, uuid, address, city, country, phone, email } = req.body;

    console.log(req.body);

    User.create({ username, password, stateId, localGovId, title, firstName, middleName, lastName, uuid, address, city, country, phone, email })
        .then(id => res.status(200).json({ id }))
        .catch(error => {
            console.log(error);
            res.status(500).json({message: "There was a problem adding the information to the database."});
        });
});

// RETURNS ALL THE USERS IN THE DATABASE
router.get('/', function (req, res) {
    User.getAll()
        .then(users => res.status(200).json(users))
        .catch(error => {
            console.log(error);
            res.status(500).json({message: "There was a problem loading the information from the database."});
        });
});

router.post('/login', function(req, res) {

    User.login(req.body.username)
        .then(users => {

            if(users.length === 0) {
                return res.status(404).send('User not found.');
            }

            //there can be only one user with the specified email
            const user = users[0];

            // check if the password is valid
            const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
            if (!passwordIsValid) return res.status(401).json({ auth: false, token: null });

            // if user is found and password is valid
            // create a token
            const token = jwt.sign({ id: user.id }, process.env.SECRET_KEY, {
                expiresIn: 86400 // expires in 24 hours
            });

            delete user.password;
            // return the information including token as JSON
            res.status(200).json({ auth: true, token: token, user });

        })
        .catch(error => {
            console.log(error);
            res.status(500).json({message: 'Error on the server.'});
        });
});

router.get('/wallet', function (req, res) {
    UserWallet.getByUserId(req.user.id)
      .then(wallet => res.status(200).json(wallet))
      .catch(error => {
        console.log(error);
        res.status(500).json({message: "There was a problem loading the information from the database."});
      });
});



module.exports = router;
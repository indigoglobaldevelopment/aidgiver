/**
 * Created by pajicv on 1/4/19.
 */
const knex = require('../db');

const TABLE_NAME = 'user_wallet';

const get = id => knex(TABLE_NAME).select().where('id', '=', id).then(wallets => wallets[0]);

const getByUserId = (userId, trx) => trx
  ? knex(TABLE_NAME).select().transacting(trx).where('user_id', '=', userId).then(wallets => wallets[0])
  : knex(TABLE_NAME).select().where('user_id', '=', userId).then(wallets => wallets[0]);

const updateBalance = (id, amount, trx) => knex(TABLE_NAME).where('id', '=', id).increment('balance', amount).transacting(trx);

const updateBalanceForUser = (userId, amount, trx) => knex(TABLE_NAME).where('user_id', '=', userId).increment('balance', amount).transacting(trx);

const create = (userId, trx) => knex(TABLE_NAME).returning('id').insert({ user_id: userId, currency: 'USD', balance: 0, decimals: 2 }).transacting(trx);

module.exports = { get, getByUserId, updateBalance, create, updateBalanceForUser };
/**
 * Created by pajicv on 1/3/19.
 */
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

router.use(bodyParser.json());
const SalePoint = require('./SalePoint');
const SalePointWallet = require('./SalePointWallet');

const jwtMiddleware = require('express-jwt');

router.post('/', /*jwtMiddleware({ secret: process.env.SECRET_KEY}),*/ function (req, res) {

    const { username, password, stateId, localGovId, name, uuid, address, city, country, phone, email, latitude, longitude  } = req.body;

    const newUsername = username || name.toLowerCase().split(' ').join('.');

    const newPassword = password || '12345678';

    SalePoint.create({  username: newUsername, password: newPassword, stateId, localGovId, name, uuid, address, city, country, phone, email, latitude, longitude })
        .then(salePointId => {
            res.status(200).json(salePointId[0])
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({message: "There was a problem adding the information to the database."});
        });
    
});

router.delete('/', /*jwtMiddleware({ secret: process.env.SECRET_KEY}),*/ function (req, res) {
    SalePoint.remove(req.body.selected)
        .then(() => {
            res.status(200).json({ success: true })
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({message: "There was a problem removing the information from the database."});
        });
});

router.get('/', /*jwtMiddleware({ secret: process.env.SECRET_KEY}),*/ function (req, res) {
    SalePoint.getAll()
        .then(salePoints => res.status(200).json(salePoints))
        .catch(error => {
            console.log(error);
            res.status(500).json({message: "There was a problem loading the information from the database."});
        });
});

router.get('/:id', /*jwtMiddleware({ secret: process.env.SECRET_KEY}),*/ function(req, res) {
    SalePoint.getById(req.params.id)
        .then(salePoints => res.status(200).json(salePoints))
        .catch(error => {
            console.log(error);
            res.status(500).json({message: "There was a problem loading the information from the database."});
        });
});



router.post('/login', function(req, res) {

    SalePoint.login(req.body.username)
        .then(salePoints => {

            if(salePoints.length === 0) {
                return res.status(404).send('Anchor not found.');
            }

            //there can be only one user with the specified email
            const salePoint = salePoints[0];

            // check if the password is valid
            const passwordIsValid = bcrypt.compareSync(req.body.password, salePoint.password);
            if (!passwordIsValid) return res.status(401).json({ auth: false, token: null });

            // if user is found and password is valid
            // create a token
            const token = jwt.sign({ id: salePoint.id }, process.env.SECRET_KEY, {
                expiresIn: 86400 // expires in 24 hours
            });

            delete salePoint.password;
            // return the information including token as JSON
            res.status(200).json({ auth: true, token: token, salePoint });

        })
        .catch(error => {
            console.log(error);
            res.status(500).json({message: 'Error on the server.'});
        });
});

module.exports = router;
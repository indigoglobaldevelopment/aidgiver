/**
 * Created by pajicv on 1/8/19.
 */
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

router.use(bodyParser.json());

const Payment = require('./Payment');

router.post('/accept', function (req, res) {

    const { applications, paymentDetails } = req.body;

    Payment.accept(paymentDetails, applications)
        .then(result => {
            console.log(result);
            res.status(200).json({message: 'Payment successfully processed'})
        })
        .catch(error => {
            console.log(error);
            res.status(400).json({message: error.message})
        });

});

router.post('/reject', function (req, res) {

    const { applications } = req.body;

    Payment.reject(applications)
        .then(result => {
            console.log(result);
            res.status(200).json({message: 'Applications successfully rejected'})
        })
        .catch(error => {
            console.log(error);
            res.status(400).json({message: error.message})
        });

});

router.get('/', function (req, res) {

    Payment.getAll()
        .then(result => {
            console.log(result);
            res.status(200).json(result)
        })
        .catch(error => {
            console.log(error);
            res.status(400).json({message: error.message})
        });

});

module.exports = router;
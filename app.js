/**
 * Created by pajicv on 1/3/19.
 */
const path = require('path');
const express = require('express');
const app = express();
const db = require('./db');
global.__root   = __dirname + '/';

app.use(express.static('public/build'));

const jwtMiddleware = require('express-jwt');

app.use(jwtMiddleware({ secret: process.env.SECRET_KEY}).unless({path: ['/api/user/login', '/api/auth/login', '/api/sale_point/login', '/dashboard', '/aplications']}));

app.get('/api', function (req, res) {
    res.status(200).send('API works.');
});

app.get("*", (req, res, next) => {
    if(req.url.substr(1,3) === 'api') {
        return next();
    }
    res.sendFile(path.resolve(__dirname + '/public/build', "index.html"));
});

const UserController = require(__root + 'user/UserController');
app.use('/api/user', UserController);

const SalePointController = require(__root + 'sale_point/SalePointController');
app.use('/api/sale_point', SalePointController);

const TransactionController = require(__root + 'transaction/TransactionController');
app.use('/api/transaction', TransactionController);

const PaymentController = require(__root + 'payment/PaymentController');
app.use('/api/payment', PaymentController);

const ApplicationController = require(__root + 'application/ApplicationController');
app.use('/api/application', ApplicationController);

const AuthController = require(__root + 'auth/AuthController');
app.use('/api/auth', AuthController);

const ClientController = require(__root + 'clients/ClientController');
app.use('/api/client', ClientController);

const AdminController = require(__root + 'admin/AdminController');
app.use('/api/admin', AdminController);

const USSDController = require(__root + 'ussd/USSDController');
app.use('/api/ussd', USSDController);

module.exports = app;


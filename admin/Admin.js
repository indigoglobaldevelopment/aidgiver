/**
 * Created by pajicv on 1/3/19.
 */

const bcrypt = require("bcryptjs");

const knex = require("../db");

const login = username =>
  knex
    .select(
      "admin.id",
      "admin.username",
      "admin.password",
      "admin.email",
      { firstName: "admin.first_name" },
      { middleName: "admin.middle_name" },
      { lastName: "admin.last_name" },
      "admin.phone",
    )
    .from("admin")
    .where("username", "=", username);

const create = ({
  username,
  password,
  stateId,
  localGovId,
  firstName,
  middleName,
  lastName,
  phone,
  email
}) => {
  const hashedPassword = bcrypt.hashSync(password, 8);

    let userId;

  return knex.transaction(trx => {
    knex("admin")
      .returning("id")
      .insert({
        username,
        password: hashedPassword,
        state_id: stateId,
        local_gov_id: localGovId,
        first_name: firstName,
        middle_name: middleName,
        last_name: lastName,
        phone,
        email
      })
      .then(ids => {
        userId = ids[0];
        return userId;
      })
      .then(trx.commit)
      .catch(trx.rollback);
  });
};

module.exports = { login, create };

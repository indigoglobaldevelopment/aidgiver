
exports.up = function(knex, Promise) {
    return knex.schema.createTable('user', function(table) {
        table.increments();
        table.string('email').notNullable();
        table.string('password').notNullable();
        table.string('username').notNullable();
        table.integer('state_id').references('id').inTable('state');
        table.integer('local_gov_id').references('id').inTable('local_gov');
        table.string('title');
        table.string('first_name');
        table.string('middle_name');
        table.string('last_name');
        table.string('uuid');
        table.string('address');
        table.string('city');
        table.string('country');
        table.string('phone');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('user');
};

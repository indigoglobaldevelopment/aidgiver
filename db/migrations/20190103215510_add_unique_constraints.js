
exports.up = function(knex, Promise) {
    return Promise.all([knex.schema.alterTable('sale_point', function(table) {
        table.unique('email');
        table.unique('username');
        table.unique('uuid');
    }), knex.schema.alterTable('user', function(table) {
        table.unique('email');
        table.unique('username');
        table.unique('uuid');
    })]);
};

exports.down = function(knex, Promise) {
    return Promise.all([knex.schema.alterTable('sale_point', function(table) {
        table.dropUnique('email');
        table.dropUnique('username');
        table.dropUnique('uuid');
    }), knex.schema.alterTable('user', function(table) {
        table.dropUnique('email');
        table.dropUnique('username');
        table.dropUnique('uuid');
    })]);
};

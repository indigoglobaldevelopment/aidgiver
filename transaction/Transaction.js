/**
 * Created by pajicv on 1/8/19.
 */

const knex = require('../db');

const UserWallet = require('../user/UserWallet');

const SalePointWallet = require('../sale_point/SalePointWallet');

const create = (userId, salePointId, amount) => {

    if(amount <= 0) {
        throw new Error('Amount have to be greather than 0');
    }

    let userWalletId;
    let salePointWalletId;
    return knex.transaction(trx => {
        UserWallet.getByUserId(userId, trx)
            .then(userWallet => {
                userWalletId = userWallet.id;
                return SalePointWallet.getBySalePointId(salePointId, trx);
            })
            .then(salePointWallet => {
                salePointWalletId = salePointWallet.id;
                return Promise.all([
                    UserWallet.updateBalance(userWalletId, -amount, trx),
                    SalePointWallet.updateBalance(salePointWalletId, amount, trx)
                ]);
            })
            .then(() =>  {
                return knex('trade')
                        .returning('id')
                        .insert({
                            user_wallet_id: userWalletId,
                            sale_point_wallet_id: salePointWalletId,
                            amount: amount
                        })
                        .transacting(trx)
            })
            .then(trx.commit)
            .catch(trx.rollback);
    });
};

const getHistoryForUser = userId =>
    knex
        .select(
            { salePointName: "sale_point.name" },
            { salePointAddress: "sale_point.address" },
            { salePointCity: "sale_point.city" },
            { salePointCountry: "sale_point.country" },
            { amount: "trade.amount" },
            { dateOfTransaction: "trade.created_at" },
        )
        .from("trade")
        .innerJoin("user_wallet", "user_wallet.id", "trade.user_wallet_id")
        .innerJoin("user", "user.id", "user_wallet.user_id")
        .innerJoin("sale_point_wallet", "sale_point_wallet.id", "trade.sale_point_wallet_id")
        .innerJoin("sale_point", "sale_point.id", "sale_point_wallet.sale_point_id")
        .where('user.id', '=', userId);

const getHistoryForSalePoint = salePointId =>
    knex
        .select(
            { userFirstName: "user.first_name" },
            { userLastName: "user.last_name" },
            { userMiddleName: "user.middle_name" },
            { amount: "trade.amount" },
            { dateOfTransaction: "trade.created_at" },
        )
        .from("trade")
        .innerJoin("user_wallet", "user_wallet.id", "trade.user_wallet_id")
        .innerJoin("user", "user.id", "user_wallet.user_id")
        .innerJoin("sale_point_wallet", "sale_point_wallet.id", "trade.sale_point_wallet_id")
        .innerJoin("sale_point", "sale_point.id", "sale_point_wallet.sale_point_id")
        .where('sale_point.id', '=', salePointId);

module.exports = { create, getHistoryForUser, getHistoryForSalePoint };
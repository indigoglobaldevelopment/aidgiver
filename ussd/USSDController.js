const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded());

const mainMenu = `CON Please select:\n 1. Check balance\n2. Pay\n3. History of payments`;

const amountMessage = 'CON Enter the amount';

const salePointMessage = 'CON Enter sale point';

const getFinishPaymentMessage = (amount, salePoint) => `END Successfully paid ${parseFloat(amount).toFixed(2)}$ to ${salePoint}`;

const getCheckBalanceMessage = (balance) => `END Your balance is ${balance.toFixed(2)}$`;

const getHistoryOfPaymentMessage = (date, salePoint, amount) => `END On ${date} ${balance > 0 ?  `received ${amount.toFixed(2)}$ from` : `paid ${amount.toFixed(2)}$ to`} ${salePoint}`;

router.post('/', function (req, res) {

  console.log(req.body);

  const { text } = req.body;

  res.set('Content-Type', 'text/plain');

  if (text === '') {

      res.status(200).send(mainMenu);

  } else if (text === '1') {

    res.status(200).send(getCheckBalanceMessage(1000.00));

  } else if (text === '2') {

    res.status(200).send(amountMessage);

  } else if (text.substr(0,2) === '2*') {

    const parts = text.split('*');

    console.log(parts);

    if(parts.length === 2) {

      res.status(200).send(salePointMessage);

    } else if (parts.length === 3) {

      res.status(200).send(getFinishPaymentMessage(parts[1], parts[2]));

    } else {

      res.status(200).send(mainMenu);

    }

  } else if (text === '3') {

    res.status(200).send(mainMenu);

  }

});

module.exports = router;
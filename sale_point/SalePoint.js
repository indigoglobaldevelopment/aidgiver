/**
 * Created by pajicv on 1/3/19.
 */

const bcrypt = require("bcryptjs");

const knex = require("../db");

const SalePointWallet = require("./SalePointWallet");

const TABLE_NAME = "sale_point";

const getAll = () =>
  knex
    .select(
      "sale_point.id",
      "sale_point.username",
      "sale_point.email",
      "sale_point.name",
      "sale_point.uuid",
      "sale_point.address",
      "sale_point.city",
      "sale_point.country",
      "sale_point.phone",
      "sale_point.latitude",
      "sale_point.longitude",
      { state: "state.name" },
      { localGov: "local_gov.name" }
    )
    .from(TABLE_NAME)
    .innerJoin("state", "state.id", "sale_point.state_id")
    .innerJoin("local_gov", "local_gov.id", "sale_point.local_gov_id");

const login = username =>
  knex
    .select(
      "sale_point.id",
      "sale_point.username",
      "sale_point.password",
      "sale_point.email",
      "sale_point.name",
      "sale_point.uuid",
      "sale_point.address",
      "sale_point.city",
      "sale_point.country",
      "sale_point.phone",
      "sale_point.latitude",
      "sale_point.longitude",
      { walletId: "sale_point_wallet.id" },
      { walletBalance: "sale_point_wallet.balance" },
      { walletCurrency: "sale_point_wallet.currency" },
      { walletDecimals: "sale_point_wallet.decimals" },
      { state: "state.name" },
      { localGov: "local_gov.name" }
    )
    .from(TABLE_NAME)
    .innerJoin(
      "sale_point_wallet",
      "sale_point.id",
      "sale_point_wallet.sale_point_id"
    )
    .innerJoin("state", "state.id", "sale_point.state_id")
    .innerJoin("local_gov", "local_gov.id", "sale_point.local_gov_id")
    .where("username", "=", username);

const getById = id =>
  knex
    .select()
    .from(TABLE_NAME)
    .innerJoin(
      "sale_point_wallet",
      "sale_point.id",
      "sale_point_wallet.sale_point_id"
    )
    .where("sale_point.id", "=", id);

const create = ({
  username,
  password,
  stateId,
  localGovId,
  name,
  uuid,
  address,
  city,
  country,
  phone,
  email,
  latitude,
  longitude
}) => {
  const hashedPassword = bcrypt.hashSync(password, 8);

  return knex.transaction(trx => {
    knex(TABLE_NAME)
      .returning("id")
      .insert({
        username,
        password: hashedPassword,
        state_id: stateId,
        local_gov_id: localGovId,
        name,
        uuid,
        address,
        city,
        country,
        phone,
        email,
        latitude,
        longitude
      })
      .transacting(trx)
      .then(ids => {
        const salePointId = ids[0];
        return SalePointWallet.create(salePointId, trx);
      })
      .then(trx.commit)
      .catch(trx.rollback);
  });
};

const remove = selected => knex(TABLE_NAME).del().whereIn('id', selected);

module.exports = { getAll, login, create, remove, getById };

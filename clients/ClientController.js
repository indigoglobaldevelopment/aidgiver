/**
 * Created by pajicv on 1/3/19.
 */
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

router.use(bodyParser.json());
const Client = require('./Client');

// CREATES A NEW CLIENT
router.post('/', function (req, res) {

  const { username, password, stateId, localGovId, title, firstName, middleName, lastName, uuid, address, city, country, phone, email } = req.body;

  console.log(req.body);

  Client.create({ username, password, stateId, localGovId, title, firstName, middleName, lastName, uuid, address, city, country, phone, email })
    .then(id => res.status(200).json({ id }))
    .catch(error => {
      console.log(error);
      res.status(500).json({message: "There was a problem adding the information to the database."});
    });
});

// RETURNS ALL THE CLIENTS IN THE DATABASE
router.get('/', function (req, res) {
  Client.getAll()
    .then(clients => res.status(200).json(clients))
    .catch(error => {
      console.log(error);
      res.status(500).json({message: "There was a problem loading the information from the database."});
    });
});

module.exports = router;
/**
 * Created by pajicv on 1/8/19.
 */
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

router.use(bodyParser.json());

const Application = require('./Application');

router.get('/total', function (req, res) {

    Application.getTotalByAppStatus()
        .then(data => res.status(200).json(data))
        .catch(error => res.status(500).json({message: error.message}))

});

router.get('/12months', function (req, res) {

    Application.getLast12MonthsByAppStatus()
        .then(data => res.status(200).json(data))
        .catch(error => res.status(500).json({message: error.message}))

});

router.get('/30days', function (req, res) {

    Application.getLast30DaysByAppStatus()
        .then(data => res.status(200).json(data))
        .catch(error => res.status(500).json({message: error.message}))

});

router.get('/state', function (req, res) {

    Application.getStates()
        .then(data => res.status(200).json(data))
        .catch(error => res.status(500).json({message: error.message}))

});

router.get('/local_gov', function (req, res) {

    Application.getLocalGovs(req.query.stateId)
        .then(data => res.status(200).json(data))
        .catch(error => res.status(500).json({message: error.message}))

});

module.exports = router;
/**
 * Created by pajicv on 1/3/19.
 */

const bcrypt = require("bcryptjs");

const knex = require("../db");

const getAll = () =>
  knex
    .select(
      "user.id",
      "user.username",
      "user.email",
      { firstName: "user.first_name" },
      "user.uuid",
      { middleName: "user.middle_name" },
      { lastName: "user.last_name" },
      "user.title",
      "user.address",
      "user.city",
      "user.country",
      "user.phone",
      { state: "state.name" },
      { localGov: "local_gov.name" }
    )
    .from("user")
    .innerJoin("state", "state.id", "user.state_id")
    .innerJoin("local_gov", "local_gov.id", "user.local_gov_id");

module.exports = { getAll };

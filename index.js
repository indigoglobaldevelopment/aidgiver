/**
 * Created by pajicv on 1/3/19.
 */
require('dotenv').config();
const fs = require('fs');
const http = require('http');
const https = require('https');
const app = require('./app');
const port = process.env.PORT || 3000;

let server;
if(process.env.NODE_ENV === 'production') {
    const privateKey  = fs.readFileSync(process.env.HTTPS_KEY_PATH, 'utf8');
    const certificate = fs.readFileSync(process.env.HTTPS_CERTIFICATE_PATH, 'utf8');
    const credentials = {key: privateKey, cert: certificate};
    server = https.createServer(credentials, app);
} else {
    server = http.createServer(app);
}

server.listen(port, function() {
    console.log('Express server listening on port ' + port);
});
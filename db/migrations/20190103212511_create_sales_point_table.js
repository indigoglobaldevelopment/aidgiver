
exports.up = function(knex, Promise) {
    return knex.schema.createTable('sale_point', function(table) {
        table.increments();
        table.string('email').notNullable();
        table.string('password').notNullable();
        table.string('username').notNullable();
        table.integer('state_id').references('id').inTable('state');
        table.integer('local_gov_id').references('id').inTable('local_gov');
        table.string('name').notNullable();
        table.string('uuid');
        table.string('address');
        table.string('city');
        table.string('country');
        table.string('phone');
        table.float('latitude', 10, 6);
        table.float('longitude', 10, 6);
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('sale_point');
};
